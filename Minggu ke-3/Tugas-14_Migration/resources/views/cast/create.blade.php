@extends('master')
@section('content')
    <h1>Halaman Create Cast</h1>
    
    
    <section class="content">

        <!-- Default box -->
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Tambah Data Cast</h3>
    
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
              </button>
              <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
            <div class="row">
                <div class="col-10">
                    <form action="/cast" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="title">Nama</label>
                            <input type="text" class="form-control" name="nama" id="title" placeholder="Masukkan Title">
                            @error('nama')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="title_umur">Umur</label>
                            <input type="number" class="form-control" name="umur" id="title_umur" min="1" max="100" placeholder="Masukkan Umur">
                            @error('umur')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="title_bio">Biodata</label>
                            <textarea class="form-control" name="bio" id="title_bio" cols="30" rows="5"></textarea>
                            {{-- <input type="text" class="form-control" name="bio" id="title" placeholder="Masukkan Bio"> --}}
                            @error('bio')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                    
                        <button type="submit" class="btn btn-primary">Tambah</button>
                    </form>
                </div>
        
            </div>
          </div>
          <!-- /.card-body -->

        </div>
        <!-- /.card -->
    
      </section>
      <!-- /.content -->
  
@endsection