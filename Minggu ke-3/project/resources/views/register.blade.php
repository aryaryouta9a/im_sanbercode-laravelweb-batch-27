<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <div class="form-section">
        <h3>Sign Up Form</h3>
        <form action="/welcome" method="post">
            @csrf
            <div class="firstname-control">
                <label for="firstname">First name:</label> <br><br>
                <input type="text" name="firstname" id="firstname" required>
            </div>
            <br>
            <div class="lastname-control">
                <label for="lastname">Last name:</label><br><br>
                <input type="text" name="lastname" id="lastname" required>
            </div>
            <br>
            <div class="gender-control">
                <label for="#">Gender:</label><br><br>

                <input type="radio" name="gender" id="male">Male
                <br>
                <input type="radio" name="gender" id="female">Female
                <br>
                <input type="radio" name="gender" id="other">Other
            </div>
            <br>
            <div class="nasionality">
                <label for="#">Nasionality:</label><br><br>
                <select name="" id="">
                    <option value="1">Indonesia</option>
                    <option value="2">Malaysia</option>
                    <option value="3">Jepang</option>
                    <option value="4">Singapura</option>
                </select>
            </div>
            <br>
            <div class="language-control">
                <label for="#">Language Spoken:</label><br><br>
                    <input type="checkbox" name="indo" id="indo">Bahasa Indonesia<br>
                    <input type="checkbox" name="english" id="english">English<br>
                    <input type="checkbox" name="other" id="other">Other
            </div>
            <br>
            <div class="bio-control">
                <label for="#">Bio:</label><br><br>
                <textarea name="bio" id="" cols="30" rows="10"></textarea>
            </div>
            <input type="submit" value="Sign Up">
        </form>
    </div>
</body>
</html>