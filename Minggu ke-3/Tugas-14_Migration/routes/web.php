<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('master');
});
Route::get('/table', function (){
    return view('pages.table');
});
Route::get('/data-tables', function (){
    return view('pages.data_tables');
});

// CRUD Cast
Route::get('/cast', 'CastController@index');
Route::get('/cast/create', 'CastController@create');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');

Route::post('/cast', 'CastController@store');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');

