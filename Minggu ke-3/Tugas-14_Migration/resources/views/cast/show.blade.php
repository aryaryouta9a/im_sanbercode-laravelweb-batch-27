@extends('master')
@section('content')
<section class="content">

    <!-- Default box -->
    <div class="card">
      <div class="card-header">
        <h3 class="card-title">Show Data Cast</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
            <i class="fas fa-minus"></i>
          </button>
          <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
            <i class="fas fa-times"></i>
          </button>
        </div>
      </div>
      <div class="card-body">
        <h2>Show Cast {{$cast->id}}</h2>
        <p></p>
        <p></p>
        <div class="card" style="width: 18rem;">
            {{-- <img src="..." class="card-img-top bg-secondary" alt="..."> --}}
            <div class="foto-profile center">
                <i class="bi bi-person-circle"></i>
            </div>
            <ul class="list-group list-group-flush">
              <li class="list-group-item">Nama : {{$cast->nama}} </li>
              <li class="list-group-item">Umur : {{$cast->umur}}</li>
              <li class="list-group-item">Biodata : <p>{{$cast->bio}}</p> </li>
            </ul> 
            <div class="card-body">
                <a href="/cast/{{$cast->id}}/edit" class="btn btn-sm btn-primary">Edit</a>
                <form action="/cast/{{$cast->id}}" method="POST" style="display:inline-block">
                    @csrf
                    @method('DELETE')
                    <input type="submit" class="btn btn-sm btn-danger my-1" value="Delete">
                </form>
            </div>
          </div>
    
      </div>
      <!-- /.card-body -->

    </div>
    <!-- /.card -->

  </section>
  <!-- /.content -->
@endsection